import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'modal-criar-sala',
  templateUrl: 'modal-criar-sala.html'
})
export class ModalCriarSalaComponent {
  text: string;

  constructor(
    public params: NavParams,
    public viewCtrl: ViewController
  ) {

  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
