import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'modal-buscar-contato',
  templateUrl: 'modal-buscar-contato.html'
})
export class ModalBuscarContatoComponent {
  items;

  text: string;

  constructor(
    public params: NavParams,
    public viewCtrl: ViewController
  ) {
    this.initializeItems();
  }
  
  getRandom(arr, n) {
    var result = new Array(n),
        len = arr.length,
        taken = new Array(len);
    if (n > len)
        throw new RangeError("getRandom: more elements taken than available");
    while (n--) {
        var x = Math.floor(Math.random() * len);
        result[n] = arr[x in taken ? taken[x] : x];
        taken[x] = --len;
    }
    return result;
  }

  initializeItems() {
    this.items = [
      {
        name: "Bo Peep",
        avatar: "assets/img/avatar-ts-bopeep.png",
        message: "Hey there! I am using Chat Agência Estado",
        time: "Seg"
      },
      {
        name: "Sarge",
        avatar: "assets/img/avatar-ts-sarge.png",
        message: "Hey there! I am using Chat Agência Estado",
        time: "Qua"
      },
      {
        name: "Squeeze",
        avatar: "assets/img/avatar-ts-squeeze.png",
        message: "Hey there! I am using Chat Agência Estado",
        time: "Sex"
      },

      {
        name: "Barbie",
        avatar: "assets/img/avatar-ts-barbie.png",
        message: "Hey there! I am using Chat Agência Estado",
        time: "Dom"
      },
      {
        name: "Bullseye",
        avatar: "assets/img/avatar-ts-bullseye.png",
        message: "Hey there! I am using Chat Agência Estado",
        time: "2:08"
      },
      {
        name: "Rex",
        avatar: "assets/img/avatar-ts-rex.png",
        message: "Hey there! I am using Chat Agência Estado",
        time: "7:22"
      },
      {
        name: "Slinky Dog",
        avatar: "assets/img/avatar-ts-slinky.png",
        message: "Hey there! I am using Chat Agência Estado",
        time: "20:54"
      },

      {
        name: "Hamm",
        avatar: "assets/img/avatar-ts-hamm.png",
        message: "Hey there! I am using Chat Agência Estado",
        time: "23:11"
      },
      {
        name: "Mr. Potato Head",
        avatar: "assets/img/avatar-ts-potatohead.png",
        message: "Hey there! I am using Chat Agência Estado",
        time: "5:47"
      },
      {
        name: "Jessie",
        avatar: "assets/img/avatar-ts-jessie.png",
        message: "Hey there! I am using Chat Agência Estado",
        time: "10:03"
      },
      {
        name: "Buzz Lightyear",
        avatar: "assets/img/avatar-ts-buzz.png",
        message: "Hey there! I am using Chat Agência Estado",
        time: "13:12"
      },
      {
        name: "Woody",
        avatar: "assets/img/avatar-ts-woody.png",
        message: "Hey there! I am using Chat Agência Estado",
        time: "15:43"
      }
    ];    
  
    //let numberRamdom = parseInt((Math.random() * 10).toFixed(0));

    this.items = this.getRandom(this.items, 12);
  }

  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the ev target
    var val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
