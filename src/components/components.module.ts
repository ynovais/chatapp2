import { NgModule } from '@angular/core';
import { ModalCriarSalaComponent } from './modal-criar-sala/modal-criar-sala';
import { ModalBuscarContatoComponent } from './modal-buscar-contato/modal-buscar-contato';
import { EmojiPickerComponent } from './emoji-picker/emoji-picker';

@NgModule({
	declarations: [ModalCriarSalaComponent, ModalBuscarContatoComponent,
    EmojiPickerComponent],
	imports: [],
	exports: [ModalCriarSalaComponent, ModalBuscarContatoComponent,
    EmojiPickerComponent]
})

export class ComponentsModule {}
