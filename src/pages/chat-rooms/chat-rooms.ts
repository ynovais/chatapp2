import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { ModalCriarSalaComponent } from "../../components/modal-criar-sala/modal-criar-sala";
import { ChatPage } from "../chat/chat";

@Component({
  selector: 'page-chat-rooms',
  templateUrl: 'chat-rooms.html',
})
export class ChatRoomsPage {
  items;
  item;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public modalCtrl: ModalController
    ) {
    this.initializeItems();
  }

  initializeItems() {
    this.items = [
      {
        name: "Sala Woody",
        descricao: "Descrição da Sala Woody",
        topico: "Tópico da sala Woody",
        avatar: "assets/img/avatar-ts-woody.png",
        message: "This town ain't big enough for the two of us!",
        time: "15:43"
      },
      {
        name: "Sala Buzz Lightyear",
        descricao: "Descrição da Sala Buzz Lightyear",
        topico: "Tópico da sala Lightyear",
        avatar: "assets/img/avatar-ts-buzz.png",
        message: "My eyeballs could have been sucked from their sockets!",
        time: "13:12"
      },
      {
        name: "Sala Jessie",
        descricao: "Descrição da Sala Jessie",
        topico: "Tópico da sala Jessie",
        avatar: "assets/img/avatar-ts-jessie.png",
        message: "Well aren't you just the sweetest space toy I ever did meet!",
        time: "10:03"
      },
      {
        name: "Sala Mr. Potato Head",
        descricao: "Descrição da Sala Mr. Potato Head",
        topico: "Tópico da sala Mr. Potato Head",
        avatar: "assets/img/avatar-ts-potatohead.png",
        message: "You're not turning me into a Mashed Potato.",
        time: "5:47"
      }
    ];

  }

  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the ev target
    var val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  openModal() {
    let modal = this.modalCtrl.create(ModalCriarSalaComponent);
    modal.present();
  }

  goToChat(item) {
    this.navCtrl.push(ChatPage, {
      toUserName: item.name
    });
  }

}
