import { Component, OnInit } from '@angular/core';
import { NavController, ModalController, NavParams } from 'ionic-angular';
import { ModalBuscarContatoComponent } from "../../components/modal-buscar-contato/modal-buscar-contato";
import { ChatPage } from "../chat/chat";
import { ChatServiceProvider } from '../../providers/chat-service/chat-service';
// import { Strophe } from 'strophe'

declare var Strophe:any;

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage implements OnInit {

   ngOnInit(): void {
    console.log('ContactPage OnInit');
    console.log(Strophe);
  //   this.connection = new Strophe.Connection("https://xmpp-local.oragon.io:5280/http-bind", {'keepalive': true});
  //   this.connection.connect("admin@xmpp-local.oragon.io", "password1234")
   }
  // xmpp_host: any = "Marlons-MacBook-Pro.local";
  // user: any = "user";
  // pass: any = "user";

  // connection: any;

//////////######################
  items;
  item;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public XMPPService: ChatServiceProvider
    ) {
    this.initializeItems();
  }

  initializeItems() {
    //alert();
      this.XMPPService
           .listusers()
            //.then(res => {
            //    this.msgList = res;
            //})
            //.catch(err => {
            //    console.log(err)
            //})
    this.items = [
      {
        name: "Woody",
        avatar: "assets/img/avatar-ts-woody.png",
        message: "Hey there! I am using Chat Agência Estado",
        status: "online",
        color: "secondary"
      },
      {
        name: "Buzz Lightyear",
        avatar: "assets/img/avatar-ts-buzz.png",
        message: "Hey there! I am using Chat Agência Estado",
        status: "ocupado",
        color: "danger"
      },
      {
        name: "Jessie",
        avatar: "assets/img/avatar-ts-jessie.png",
        message: "Hey there! I am using Chat Agência Estado",
        status: "ausente",
        color: "warning"
      },
      {
        name: "Mr. Potato Head",
        avatar: "assets/img/avatar-ts-potatohead.png",
        message: "Hey there! I am using Chat Agência Estado",
        status: "offline",
        color: "light"
      },

      {
        name: "Hamm",
        avatar: "assets/img/avatar-ts-hamm.png",
        message: "Hey there! I am using Chat Agência Estado",
        status: "offline",
        color: "light"
      },
      {
        name: "Slinky Dog",
        avatar: "assets/img/avatar-ts-slinky.png",
        message: "Hey there! I am using Chat Agência Estado",
        status: "ausente",
        color: "warning"
      },
      {
        name: "Rex",
        avatar: "assets/img/avatar-ts-rex.png",
        message: "Hey there! I am using Chat Agência Estado",
        status: "ocupado",
        color: "danger"
      },
      {
        name: "Bullseye",
        avatar: "assets/img/avatar-ts-bullseye.png",
        message: "Hey there! I am using Chat Agência Estado",
        status: "online",
        color: "secondary"
      },

      {
        name: "Barbie",
        avatar: "assets/img/avatar-ts-barbie.png",
        message: "Hey there! I am using Chat Agência Estado",
        status: "oculpado",
        color: "danger"
      },
      {
        name: "Squeeze",
        avatar: "assets/img/avatar-ts-squeeze.png",
        message: "Hey there! I am using Chat Agência Estado",
        status: "ausente",
        color: "warning"
      },
      {
        name: "Sarge",
        avatar: "assets/img/avatar-ts-sarge.png",
        message: "Hey there! I am using Chat Agência Estado",
        status: "offline",
        color: "light"
      },
      {
        name: "Bo Peep",
        avatar: "assets/img/avatar-ts-bopeep.png",
        message: "Hey there! I am using Chat Agência Estado",
        status: "online",
        color: "secondary"
      }
    ];

  }

  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the ev target
    var val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  openModal() {
    let modal = this.modalCtrl.create(ModalBuscarContatoComponent);
    modal.present();
  }

  goToChat(item) {
    this.navCtrl.push(ChatPage, {
      toUserName: item.name
    });
  }

}
