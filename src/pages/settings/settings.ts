import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SettingStatusPage } from "../setting-status/setting-status";
import { SettingMessagePage } from "../setting-message/setting-message";

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  goToStatus(){
    this.navCtrl.push(SettingStatusPage);
  }

  goToMessage(){
    this.navCtrl.push(SettingMessagePage);
  }

}
