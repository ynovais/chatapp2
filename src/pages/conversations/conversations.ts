import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ChatPage } from "../chat/chat";


@Component({
  selector: 'page-conversations',
  templateUrl: 'conversations.html',
})
export class ConversationsPage {
  items;
  item;

  toUser:Object;

  constructor(public navCtrl: NavController) {
    this.initializeItems();
    this.toUser = {
      toUserId:'210000198410281948',
      toUserName:'Hancock'
    }
  }

  initializeItems() {
    this.items = [
      {
        name: "Hamm",
        avatar: "assets/img/avatar-ts-hamm.png",
        message: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        time: "23:11"
      },
      {
        name: "Slinky Dog",
        avatar: "assets/img/avatar-ts-slinky.png",
        message: "In vehicula sapien tempus volutpat suscipit.",
        time: "20:54"
      },
      {
        name: "Rex",
        avatar: "assets/img/avatar-ts-rex.png",
        message: "Vestibulum molestie pretium pulvinar.",
        time: "7:22"
      },
      {
        name: "Bullseye",
        avatar: "assets/img/avatar-ts-bullseye.png",
        message: "Aenean dictum a nunc",
        time: "2:08"
      },

      {
        name: "Woody",
        avatar: "assets/img/avatar-ts-woody.png",
        message: "Integer ipsum tortor",
        time: "15:43"
      },
      {
        name: "Buzz Lightyear",
        avatar: "assets/img/avatar-ts-buzz.png",
        message: "Nulla ante velit, mollis eget erat non",
        time: "13:12"
      },
      {
        name: "Jessie",
        avatar: "assets/img/avatar-ts-jessie.png",
        message: "Praesent sit amet tincidunt massa.",
        time: "10:03"
      },
      {
        name: "Mr. Potato Head",
        avatar: "assets/img/avatar-ts-potatohead.png",
        message: "Donec imperdiet dui sit amet consectetur pharetra",
        time: "5:47"
      },

      {
        name: "Barbie",
        avatar: "assets/img/avatar-ts-barbie.png",
        message: "Vivamus accumsan lectus",
        time: "Dom"
      },
      {
        name: "Squeeze",
        avatar: "assets/img/avatar-ts-squeeze.png",
        message: "Pellentesque fermentum sagittis cursus",
        time: "Sex"
      },
      {
        name: "Sarge",
        avatar: "assets/img/avatar-ts-sarge.png",
        message: "Aenean neque purus",
        time: "Qua"
      },
      {
        name: "Bo Peep",
        avatar: "assets/img/avatar-ts-bopeep.png",
        message: "Donec rhoncus enim neque, et luctus enim tincidunt ut.",
        time: "Seg"
      }
    ];

  }

  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the ev target
    var val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  goToChat(item) {
    this.navCtrl.push(ChatPage, {
      toUserName: item.name
    });
  }

}
