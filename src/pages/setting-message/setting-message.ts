import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-setting-message',
  templateUrl: 'setting-message.html',
})
export class SettingMessagePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingMessagePage');
  }

  goBack() {
    this.navCtrl.pop();
  }

}
