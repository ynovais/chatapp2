import { Component } from '@angular/core';

import { ContactPage } from '../contact/contact';
import { ConversationsPage } from '../conversations/conversations';
import { ChatRoomsPage } from '../chat-rooms/chat-rooms';
import { SettingsPage } from '../settings/settings';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = ContactPage;
  tab2Root = ConversationsPage;
  tab3Root = ChatRoomsPage;
  tab4Root = SettingsPage;

  constructor() {

  }
}
