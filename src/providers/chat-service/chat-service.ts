import { Injectable, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { Events } from 'ionic-angular';

import 'rxjs/add/operator/toPromise';



declare var Strophe:any;
declare var $iq: any;
declare var $pres: any; 
declare var $msg: any;
declare var $build: any;
declare var   connection: any ;
declare var BOSH_SERVICE: string ;
export class ChatMessage {


    messageId: string;
    userId: string;
    userName: string;
    userImgUrl: string;
    toUserId: string;
    time: number | string;
    message: string;
    status: string;
}

export class UserInfo {
    userId: string;
    userName: string;
    userImgUrl: string;
}

/*
  Generated class for the ChatServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/

@Injectable()
export class ChatServiceProvider implements OnInit {

    ngOnInit(): void {
        //console.log(Strophe);
        //alert('ChatServiceProvider oninit');
        
       // connection = new Strophe.Connection(this.BOSH_SERVICE);
     //   connection.connect('fabiola@monkey', '123mudar', this.onConnect);
    }

 
//    Strophe: any;
 //  $build: any;   
  // $iq: any;
   // $msg: any;
   // $pres: any;



  //public BOSH_SERVICE: any="https://conversejs.org/http-bind/"  ;
  //public BOSH_SERVICE: any="http://localhost:7070/http-bind/"  ;
  constructor(public http: Http,
                public events: Events
) {
    var BOSH_SERVICE = "http://localhost:7070/http-bind/";
    var  connection:any;
    //console.log('ChatServiceProvider constructor');
    console.log(Strophe.Connection);
    connection = new Strophe.Connection(BOSH_SERVICE);
    //console.log(this.connection);
    connection.connect('fabiola@monkey', '123mudar', this.onConnect);
    //console.log(Strophe);
    //console.log(JSON.stringify(strophejs))
    
    
    
    }
     connect () 
     {
         
     };
     listusers()  {       
       // var iq = $iq({
       //     type: 'get',
       //     from: 'fabiola@monkey',
       //     to: 'monkey'
       //   }).c('query', {xmlns: 'http://jabber.org/protocol/disco#items', node: 'http://jabber.org/protocol/commands'});
       //   this.connection.send(iq.tree());
    };
    listroons()  {       
        connection.muc.listRooms('monkey', function(rooms) {
            alert(rooms);
            //convert XML TO JSON
          }, function(err) {
            console.log("listRooms - error: " + err);
          });
    };
    sendmessage()  {       
        function sendMessage(msg, to) {
            var m = $msg({
              to: to,
              from: 'fabiola@monkey.com',
              type: 'chat'
            }).c("body").t(msg);
            this.connection.send(m);
          }
    };

    
        onConnect(status) {
            console.log('onConnect: '+status);
            //alert(status);
            //alert(Strophe.Status.CONNECTED); //5
            //alert(Strophe.Status.DISCONNECTED); //6
            //alert(Strophe.Status.CONNFAIL); //2
            //alert(Strophe.Status.DISCONNECTING);//7
            //alert(Strophe.Status.CONNECTING);  //1
            //alert(Strophe.Status.AUTHENTICATING);  // 3
            //alert(Strophe.Status.AUTHFAIL);  // 4
            //alert(Strophe.Status.ATTACHED);  //8
            //alert(Strophe.Status.REDIRECT);  // 9
            //alert(Strophe.Status.CONNTIMEOUT);  //10
           
            if (status == Strophe.Status.CONNECTED)
            {
                              
                var elementShow = Strophe.xmlElement('show', {}, 'chat');
                var elementStatus = Strophe.xmlElement('status', {}, 'Online');
                console.log('1');
                console.log($pres);
                var presence = $pres({from: 'fabiola@monkey', xmlns: 'jabber:client', 'xmlns:stream': 'http://etherx.jabber.org/streams', version: '1.0'})
                  .cnode(elementShow).up()
                  .cnode(elementStatus);
                  console.log(presence);
                  console.log('2');
                 // console.log(connection);
                  console.log('3');
                 // console.log(BOSH_SERVICE);
                  console.log(Strophe);
                var  connection1 = new Strophe.Connection("http://localhost:7070/http-bind/");
                   console.log(connection1);
                   console.log(presence);
                connection1.send(presence.tree());
                console.log('');

            }
        }
        
    mockNewMsg(msg) {
        const mockMsg = new ChatMessage();
        
        mockMsg.messageId = Date.now().toString();
        mockMsg.userId = '210000198410281948';
        mockMsg.userName = 'Hancock';
        mockMsg.userImgUrl = './assets/to-user.jpg';
        mockMsg.toUserId = '140000198202211138';
        mockMsg.time = Date.now();
        mockMsg.message = msg.message;
        mockMsg.status = 'success';
        

        setTimeout(() => {
            this.events.publish('chat:received', mockMsg, Date.now())
        }, Math.random() * 1800)
    }

    getMsgList(): Promise<ChatMessage[]> {
        const msgListUrl = './assets/mock/msg-list.json';
        return this.http.get(msgListUrl)
            .toPromise()
            .then(response => response.json().array as ChatMessage[])
            .catch(err => Promise.reject(err || 'err'));
    }

    sendMsg(msg: ChatMessage) {
        return new Promise(resolve => setTimeout(() => resolve(msg), Math.random() * 1000))
            .then(() => this.mockNewMsg(msg));
    }

    getUserInfo(): Promise<UserInfo> {
        const userInfo: UserInfo = {
            userId: '140000198202211138',
            userName: 'Luff',
            userImgUrl: './assets/user.jpg'
        };
        return new Promise(resolve => resolve(userInfo));
    }
    
}
