import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
//import * as Strophe from 'strophe.js';


import { ChatRoomsPage } from '../pages/chat-rooms/chat-rooms';
import { ContactPage } from '../pages/contact/contact';
import { ConversationsPage } from '../pages/conversations/conversations';
import { ChatPage } from "../pages/chat/chat";
import { SettingsPage } from '../pages/settings/settings';
import { SettingStatusPage } from "../pages/setting-status/setting-status";
import { SettingMessagePage } from "../pages/setting-message/setting-message";
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ModalCriarSalaComponent } from "../components/modal-criar-sala/modal-criar-sala";
import { ModalBuscarContatoComponent } from "../components/modal-buscar-contato/modal-buscar-contato";
import { ChatServiceProvider } from '../providers/chat-service/chat-service';
import { EmojiProvider } from '../providers/emoji/emoji';
import { PipesModule } from "../pipes/pipes.module";
import { EmojiPickerComponent } from "../components/emoji-picker/emoji-picker";
import { HttpModule } from '@angular/http';


//import 'rxjs/add/operator/map';
//import 'rxjs/add/operator/toPromise';

@NgModule({
  declarations: [
    MyApp,
    ChatRoomsPage,
    ContactPage,
    ConversationsPage,
    ChatPage,
    SettingsPage,
    SettingStatusPage,
    SettingMessagePage,
    TabsPage,
    ModalCriarSalaComponent,
    ModalBuscarContatoComponent,
    EmojiPickerComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: "Voltar"
    }),
    PipesModule,
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ChatRoomsPage,
    ContactPage,
    ConversationsPage,
    ChatPage,
    SettingsPage,
    SettingStatusPage,
    SettingMessagePage,
    TabsPage,
    ModalCriarSalaComponent,
    ModalBuscarContatoComponent,
    EmojiPickerComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ChatServiceProvider,
    EmojiProvider
  ]
})
export class AppModule {}
  