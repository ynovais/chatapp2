webpackJsonp([0],{

/***/ 100:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatMessage; });
/* unused harmony export UserInfo */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return ChatServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_toPromise__ = __webpack_require__(390);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_toPromise__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ChatMessage = (function () {
    function ChatMessage() {
    }
    return ChatMessage;
}());

var UserInfo = (function () {
    function UserInfo() {
    }
    return UserInfo;
}());

/*
  Generated class for the ChatServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var ChatServiceProvider = (function () {
    function ChatServiceProvider(http, events) {
        //console.log('ChatServiceProvider constructor');
        this.http = http;
        this.events = events;
        //    Strophe: any;
        //  $build: any;   
        // $iq: any;
        // $msg: any;
        // $pres: any;
        //public BOSH_SERVICE: any="https://conversejs.org/http-bind/"  ;
        this.BOSH_SERVICE = "http://localhost:7070/http-bind/";
        this.connection = new Strophe.Connection(this.BOSH_SERVICE);
        //console.log(this.connection);
        this.connection.connect('fabiola@monkey', '123mudar', this.onConnect);
        //console.log(Strophe);
        //console.log(JSON.stringify(strophejs))
        // this.Strophe = strophejs.Strophe;
        // this.$build = Strophe.$build;
        // this.$iq = Strophe.$iq;
        // this.$msg = Strophe.$msg;
        //this.$pres = Strophe.$pres;
        // console.log(this.Strophe);
    }
    ChatServiceProvider.prototype.ngOnInit = function () {
        console.log(Strophe);
        alert('ChatServiceProvider oninit');
    };
    ChatServiceProvider.prototype.listusers = function () {
        var iq = $iq({
            type: 'get',
            from: 'fabiola@monkey',
            to: 'monkey'
        }).c('query', { xmlns: 'http://jabber.org/protocol/disco#items', node: 'http://jabber.org/protocol/commands' });
        this.connection.send(iq.tree());
    };
    ;
    ChatServiceProvider.prototype.listroons = function () {
        this.connection.muc.listRooms('monkey', function (rooms) {
            alert(rooms);
            //convert XML TO JSON
        }, function (err) {
            console.log("listRooms - error: " + err);
        });
    };
    ;
    ChatServiceProvider.prototype.sendmessage = function () {
        function sendMessage(msg, to) {
            var m = $msg({
                to: to,
                from: 'fabiola@monkey.com',
                type: 'chat'
            }).c("body").t(msg);
            this.connection.send(m);
        }
    };
    ;
    ChatServiceProvider.prototype.onConnect = function (status) {
        console.log('onConnect: ' + status);
        //alert(status);
        //alert(Strophe.Status.CONNECTED); //5
        //alert(Strophe.Status.DISCONNECTED); //6
        //alert(Strophe.Status.CONNFAIL); //2
        //alert(Strophe.Status.DISCONNECTING);//7
        //alert(Strophe.Status.CONNECTING);  //1
        //alert(Strophe.Status.AUTHENTICATING);  // 3
        //alert(Strophe.Status.AUTHFAIL);  // 4
        //alert(Strophe.Status.ATTACHED);  //8
        //alert(Strophe.Status.REDIRECT);  // 9
        //alert(Strophe.Status.CONNTIMEOUT);  //10
        if (status == Strophe.Status.CONNECTED) {
            var elementShow = Strophe.xmlElement('show', {}, 'chat');
            var elementStatus = Strophe.xmlElement('status', {}, '我在线,来戳我嘛');
            console.log('1');
            console.log($pres);
            var presence = $pres({ from: 'fabiola@monkey', xmlns: 'jabber:client', 'xmlns:stream': 'http://etherx.jabber.org/streams', version: '1.0' })
                .cnode(elementShow).up()
                .cnode(elementStatus);
            console.log(presence);
            console.log('2');
            console.log(this.connection);
            console.log('3');
            alert(this.BOSH_SERVICE);
            // this.conn = new Strophe.Connection(this.BOSH_SERVICE);
            //this.connection.send(presence.tree());
        }
    };
    ChatServiceProvider.prototype.mockNewMsg = function (msg) {
        var _this = this;
        var mockMsg = new ChatMessage();
        mockMsg.messageId = Date.now().toString();
        mockMsg.userId = '210000198410281948';
        mockMsg.userName = 'Hancock';
        mockMsg.userImgUrl = './assets/to-user.jpg';
        mockMsg.toUserId = '140000198202211138';
        mockMsg.time = Date.now();
        mockMsg.message = msg.message;
        mockMsg.status = 'success';
        setTimeout(function () {
            _this.events.publish('chat:received', mockMsg, Date.now());
        }, Math.random() * 1800);
    };
    ChatServiceProvider.prototype.getMsgList = function () {
        var msgListUrl = './assets/mock/msg-list.json';
        return this.http.get(msgListUrl)
            .toPromise()
            .then(function (response) { return response.json().array; })
            .catch(function (err) { return Promise.reject(err || 'err'); });
    };
    ChatServiceProvider.prototype.sendMsg = function (msg) {
        var _this = this;
        return new Promise(function (resolve) { return setTimeout(function () { return resolve(msg); }, Math.random() * 1000); })
            .then(function () { return _this.mockNewMsg(msg); });
    };
    ChatServiceProvider.prototype.getUserInfo = function () {
        var userInfo = {
            userId: '140000198202211138',
            userName: 'Luff',
            userImgUrl: './assets/user.jpg'
        };
        return new Promise(function (resolve) { return resolve(userInfo); });
    };
    return ChatServiceProvider;
}());
ChatServiceProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* Events */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* Events */]) === "function" && _b || Object])
], ChatServiceProvider);

var _a, _b;
//# sourceMappingURL=chat-service.js.map

/***/ }),

/***/ 110:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 110;

/***/ }),

/***/ 152:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 152;

/***/ }),

/***/ 196:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__contact_contact__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__conversations_conversations__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__chat_rooms_chat_rooms__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__settings_settings__ = __webpack_require__(203);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TabsPage = (function () {
    function TabsPage() {
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_1__contact_contact__["a" /* ContactPage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_2__conversations_conversations__["a" /* ConversationsPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_3__chat_rooms_chat_rooms__["a" /* ChatRoomsPage */];
        this.tab4Root = __WEBPACK_IMPORTED_MODULE_4__settings_settings__["a" /* SettingsPage */];
    }
    return TabsPage;
}());
TabsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"C:\Repositories\ChatApp2\src\pages\tabs\tabs.html"*/'<ion-tabs>\n\n  <ion-tab [root]="tab1Root" tabTitle="Contatos" tabIcon="ios-contact-outline"></ion-tab>\n\n  <ion-tab [root]="tab2Root" tabTitle="Conversas" tabIcon="ios-chatbubbles-outline"></ion-tab>\n\n  <ion-tab [root]="tab3Root" tabTitle="Salas" tabIcon="ios-contacts-outline"></ion-tab>\n\n  <ion-tab [root]="tab4Root" tabTitle="Configurações" tabIcon="ios-settings-outline"></ion-tab>\n\n</ion-tabs>\n\n'/*ion-inline-end:"C:\Repositories\ChatApp2\src\pages\tabs\tabs.html"*/
    }),
    __metadata("design:paramtypes", [])
], TabsPage);

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 197:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_modal_buscar_contato_modal_buscar_contato__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__chat_chat__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_chat_service_chat_service__ = __webpack_require__(100);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ContactPage = (function () {
    function ContactPage(navCtrl, navParams, modalCtrl, XMPPService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.XMPPService = XMPPService;
        this.initializeItems();
    }
    ContactPage.prototype.ngOnInit = function () {
        console.log('ContactPage OnInit');
        console.log(Strophe);
        //   this.connection = new Strophe.Connection("https://xmpp-local.oragon.io:5280/http-bind", {'keepalive': true});
        //   this.connection.connect("admin@xmpp-local.oragon.io", "password1234")
    };
    ContactPage.prototype.initializeItems = function () {
        //alert();
        this.XMPPService
            .listusers();
        //.then(res => {
        //    this.msgList = res;
        //})
        //.catch(err => {
        //    console.log(err)
        //})
        this.items = [
            {
                name: "Woody",
                avatar: "assets/img/avatar-ts-woody.png",
                message: "Hey there! I am using Chat Agência Estado",
                status: "online",
                color: "secondary"
            },
            {
                name: "Buzz Lightyear",
                avatar: "assets/img/avatar-ts-buzz.png",
                message: "Hey there! I am using Chat Agência Estado",
                status: "ocupado",
                color: "danger"
            },
            {
                name: "Jessie",
                avatar: "assets/img/avatar-ts-jessie.png",
                message: "Hey there! I am using Chat Agência Estado",
                status: "ausente",
                color: "warning"
            },
            {
                name: "Mr. Potato Head",
                avatar: "assets/img/avatar-ts-potatohead.png",
                message: "Hey there! I am using Chat Agência Estado",
                status: "offline",
                color: "light"
            },
            {
                name: "Hamm",
                avatar: "assets/img/avatar-ts-hamm.png",
                message: "Hey there! I am using Chat Agência Estado",
                status: "offline",
                color: "light"
            },
            {
                name: "Slinky Dog",
                avatar: "assets/img/avatar-ts-slinky.png",
                message: "Hey there! I am using Chat Agência Estado",
                status: "ausente",
                color: "warning"
            },
            {
                name: "Rex",
                avatar: "assets/img/avatar-ts-rex.png",
                message: "Hey there! I am using Chat Agência Estado",
                status: "ocupado",
                color: "danger"
            },
            {
                name: "Bullseye",
                avatar: "assets/img/avatar-ts-bullseye.png",
                message: "Hey there! I am using Chat Agência Estado",
                status: "online",
                color: "secondary"
            },
            {
                name: "Barbie",
                avatar: "assets/img/avatar-ts-barbie.png",
                message: "Hey there! I am using Chat Agência Estado",
                status: "oculpado",
                color: "danger"
            },
            {
                name: "Squeeze",
                avatar: "assets/img/avatar-ts-squeeze.png",
                message: "Hey there! I am using Chat Agência Estado",
                status: "ausente",
                color: "warning"
            },
            {
                name: "Sarge",
                avatar: "assets/img/avatar-ts-sarge.png",
                message: "Hey there! I am using Chat Agência Estado",
                status: "offline",
                color: "light"
            },
            {
                name: "Bo Peep",
                avatar: "assets/img/avatar-ts-bopeep.png",
                message: "Hey there! I am using Chat Agência Estado",
                status: "online",
                color: "secondary"
            }
        ];
    };
    ContactPage.prototype.getItems = function (ev) {
        // Reset items back to all of the items
        this.initializeItems();
        // set val to the value of the ev target
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.items = this.items.filter(function (item) {
                return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    };
    ContactPage.prototype.openModal = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__components_modal_buscar_contato_modal_buscar_contato__["a" /* ModalBuscarContatoComponent */]);
        modal.present();
    };
    ContactPage.prototype.goToChat = function (item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__chat_chat__["a" /* ChatPage */], {
            toUserName: item.name
        });
    };
    return ContactPage;
}());
ContactPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-contact',template:/*ion-inline-start:"C:\Repositories\ChatApp2\src\pages\contact\contact.html"*/'<ion-header>\n\n  <ion-navbar>\n\n\n\n    <ion-title>Contatos</ion-title>\n\n\n\n    <ion-buttons end>\n\n      <button ion-button icon-only (click)="openModal()">\n\n        <ion-icon name="ios-add"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n  <ion-searchbar (ionInput)="getItems($event)"></ion-searchbar>\n\n  \n\n  <ion-list>\n\n\n\n    <ion-item *ngFor="let item of items" (click)="goToChat(item)">\n\n      <ion-avatar item-start>\n\n        <img [src]="item.avatar">\n\n        <ion-badge [color]="item.color"></ion-badge>\n\n      </ion-avatar>\n\n\n\n      <h2>{{item.name}}</h2>\n\n      <p>{{item.message}}</p>\n\n      <ion-note item-end>{{item.status}}</ion-note>\n\n    </ion-item>\n\n\n\n  </ion-list>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Repositories\ChatApp2\src\pages\contact\contact.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_4__providers_chat_service_chat_service__["b" /* ChatServiceProvider */]])
], ContactPage);

//# sourceMappingURL=contact.js.map

/***/ }),

/***/ 198:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalBuscarContatoComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ModalBuscarContatoComponent = (function () {
    function ModalBuscarContatoComponent(params, viewCtrl) {
        this.params = params;
        this.viewCtrl = viewCtrl;
        this.initializeItems();
    }
    ModalBuscarContatoComponent.prototype.getRandom = function (arr, n) {
        var result = new Array(n), len = arr.length, taken = new Array(len);
        if (n > len)
            throw new RangeError("getRandom: more elements taken than available");
        while (n--) {
            var x = Math.floor(Math.random() * len);
            result[n] = arr[x in taken ? taken[x] : x];
            taken[x] = --len;
        }
        return result;
    };
    ModalBuscarContatoComponent.prototype.initializeItems = function () {
        this.items = [
            {
                name: "Bo Peep",
                avatar: "assets/img/avatar-ts-bopeep.png",
                message: "Hey there! I am using Chat Agência Estado",
                time: "Seg"
            },
            {
                name: "Sarge",
                avatar: "assets/img/avatar-ts-sarge.png",
                message: "Hey there! I am using Chat Agência Estado",
                time: "Qua"
            },
            {
                name: "Squeeze",
                avatar: "assets/img/avatar-ts-squeeze.png",
                message: "Hey there! I am using Chat Agência Estado",
                time: "Sex"
            },
            {
                name: "Barbie",
                avatar: "assets/img/avatar-ts-barbie.png",
                message: "Hey there! I am using Chat Agência Estado",
                time: "Dom"
            },
            {
                name: "Bullseye",
                avatar: "assets/img/avatar-ts-bullseye.png",
                message: "Hey there! I am using Chat Agência Estado",
                time: "2:08"
            },
            {
                name: "Rex",
                avatar: "assets/img/avatar-ts-rex.png",
                message: "Hey there! I am using Chat Agência Estado",
                time: "7:22"
            },
            {
                name: "Slinky Dog",
                avatar: "assets/img/avatar-ts-slinky.png",
                message: "Hey there! I am using Chat Agência Estado",
                time: "20:54"
            },
            {
                name: "Hamm",
                avatar: "assets/img/avatar-ts-hamm.png",
                message: "Hey there! I am using Chat Agência Estado",
                time: "23:11"
            },
            {
                name: "Mr. Potato Head",
                avatar: "assets/img/avatar-ts-potatohead.png",
                message: "Hey there! I am using Chat Agência Estado",
                time: "5:47"
            },
            {
                name: "Jessie",
                avatar: "assets/img/avatar-ts-jessie.png",
                message: "Hey there! I am using Chat Agência Estado",
                time: "10:03"
            },
            {
                name: "Buzz Lightyear",
                avatar: "assets/img/avatar-ts-buzz.png",
                message: "Hey there! I am using Chat Agência Estado",
                time: "13:12"
            },
            {
                name: "Woody",
                avatar: "assets/img/avatar-ts-woody.png",
                message: "Hey there! I am using Chat Agência Estado",
                time: "15:43"
            }
        ];
        //let numberRamdom = parseInt((Math.random() * 10).toFixed(0));
        this.items = this.getRandom(this.items, 12);
    };
    ModalBuscarContatoComponent.prototype.getItems = function (ev) {
        // Reset items back to all of the items
        this.initializeItems();
        // set val to the value of the ev target
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.items = this.items.filter(function (item) {
                return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    };
    ModalBuscarContatoComponent.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return ModalBuscarContatoComponent;
}());
ModalBuscarContatoComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'modal-buscar-contato',template:/*ion-inline-start:"C:\Repositories\ChatApp2\src\components\modal-buscar-contato\modal-buscar-contato.html"*/'<ion-header>\n\n  <ion-toolbar>\n\n\n\n    <ion-title>\n\n      Buscar contato\n\n    </ion-title>\n\n\n\n    <ion-buttons start>\n\n      <button ion-button (click)="dismiss()">\n\n        <span ion-text color="primary">Cancel</span>\n\n      </button>\n\n    </ion-buttons>\n\n\n\n    <ion-buttons end>\n\n      <button ion-button (click)="dismiss()">\n\n        <span ion-text color="primary">OK</span>\n\n      </button>\n\n    </ion-buttons>\n\n\n\n  </ion-toolbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  \n\n  <ion-searchbar (ionInput)="getItems($event)"></ion-searchbar>\n\n  \n\n  <ion-list>\n\n\n\n    <ion-item *ngFor="let item of items">\n\n      <ion-label>\n\n        <h2>{{item.name}}</h2>\n\n        <p>{{item.message}}</p>\n\n      </ion-label>\n\n\n\n      <ion-checkbox></ion-checkbox>\n\n\n\n      <ion-avatar item-start>\n\n        <img [src]="item.avatar">\n\n      </ion-avatar>\n\n    </ion-item>\n\n\n\n  </ion-list>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Repositories\ChatApp2\src\components\modal-buscar-contato\modal-buscar-contato.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ViewController */]])
], ModalBuscarContatoComponent);

//# sourceMappingURL=modal-buscar-contato.js.map

/***/ }),

/***/ 200:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConversationsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__chat_chat__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ConversationsPage = (function () {
    function ConversationsPage(navCtrl) {
        this.navCtrl = navCtrl;
        this.initializeItems();
        this.toUser = {
            toUserId: '210000198410281948',
            toUserName: 'Hancock'
        };
    }
    ConversationsPage.prototype.initializeItems = function () {
        this.items = [
            {
                name: "Hamm",
                avatar: "assets/img/avatar-ts-hamm.png",
                message: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                time: "23:11"
            },
            {
                name: "Slinky Dog",
                avatar: "assets/img/avatar-ts-slinky.png",
                message: "In vehicula sapien tempus volutpat suscipit.",
                time: "20:54"
            },
            {
                name: "Rex",
                avatar: "assets/img/avatar-ts-rex.png",
                message: "Vestibulum molestie pretium pulvinar.",
                time: "7:22"
            },
            {
                name: "Bullseye",
                avatar: "assets/img/avatar-ts-bullseye.png",
                message: "Aenean dictum a nunc",
                time: "2:08"
            },
            {
                name: "Woody",
                avatar: "assets/img/avatar-ts-woody.png",
                message: "Integer ipsum tortor",
                time: "15:43"
            },
            {
                name: "Buzz Lightyear",
                avatar: "assets/img/avatar-ts-buzz.png",
                message: "Nulla ante velit, mollis eget erat non",
                time: "13:12"
            },
            {
                name: "Jessie",
                avatar: "assets/img/avatar-ts-jessie.png",
                message: "Praesent sit amet tincidunt massa.",
                time: "10:03"
            },
            {
                name: "Mr. Potato Head",
                avatar: "assets/img/avatar-ts-potatohead.png",
                message: "Donec imperdiet dui sit amet consectetur pharetra",
                time: "5:47"
            },
            {
                name: "Barbie",
                avatar: "assets/img/avatar-ts-barbie.png",
                message: "Vivamus accumsan lectus",
                time: "Dom"
            },
            {
                name: "Squeeze",
                avatar: "assets/img/avatar-ts-squeeze.png",
                message: "Pellentesque fermentum sagittis cursus",
                time: "Sex"
            },
            {
                name: "Sarge",
                avatar: "assets/img/avatar-ts-sarge.png",
                message: "Aenean neque purus",
                time: "Qua"
            },
            {
                name: "Bo Peep",
                avatar: "assets/img/avatar-ts-bopeep.png",
                message: "Donec rhoncus enim neque, et luctus enim tincidunt ut.",
                time: "Seg"
            }
        ];
    };
    ConversationsPage.prototype.getItems = function (ev) {
        // Reset items back to all of the items
        this.initializeItems();
        // set val to the value of the ev target
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.items = this.items.filter(function (item) {
                return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    };
    ConversationsPage.prototype.goToChat = function (item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__chat_chat__["a" /* ChatPage */], {
            toUserName: item.name
        });
    };
    return ConversationsPage;
}());
ConversationsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-conversations',template:/*ion-inline-start:"C:\Repositories\ChatApp2\src\pages\conversations\conversations.html"*/'<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Conversas</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n\n\n  <ion-searchbar (ionInput)="getItems($event)"></ion-searchbar>\n\n  \n\n  <ion-list>\n\n\n\n    <ion-item *ngFor="let item of items" (click)="goToChat(item)">\n\n      <ion-avatar item-start>\n\n        <img [src]="item.avatar">\n\n      </ion-avatar>\n\n      <h2>{{item.name}}</h2>\n\n      <p>{{item.message}}</p>\n\n      <ion-note item-end>{{item.time}}</ion-note>\n\n    </ion-item>\n\n\n\n  </ion-list>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Repositories\ChatApp2\src\pages\conversations\conversations.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */]])
], ConversationsPage);

//# sourceMappingURL=conversations.js.map

/***/ }),

/***/ 201:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatRoomsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_modal_criar_sala_modal_criar_sala__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__chat_chat__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ChatRoomsPage = (function () {
    function ChatRoomsPage(navCtrl, navParams, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.initializeItems();
    }
    ChatRoomsPage.prototype.initializeItems = function () {
        this.items = [
            {
                name: "Sala Woody",
                descricao: "Descrição da Sala Woody",
                topico: "Tópico da sala Woody",
                avatar: "assets/img/avatar-ts-woody.png",
                message: "This town ain't big enough for the two of us!",
                time: "15:43"
            },
            {
                name: "Sala Buzz Lightyear",
                descricao: "Descrição da Sala Buzz Lightyear",
                topico: "Tópico da sala Lightyear",
                avatar: "assets/img/avatar-ts-buzz.png",
                message: "My eyeballs could have been sucked from their sockets!",
                time: "13:12"
            },
            {
                name: "Sala Jessie",
                descricao: "Descrição da Sala Jessie",
                topico: "Tópico da sala Jessie",
                avatar: "assets/img/avatar-ts-jessie.png",
                message: "Well aren't you just the sweetest space toy I ever did meet!",
                time: "10:03"
            },
            {
                name: "Sala Mr. Potato Head",
                descricao: "Descrição da Sala Mr. Potato Head",
                topico: "Tópico da sala Mr. Potato Head",
                avatar: "assets/img/avatar-ts-potatohead.png",
                message: "You're not turning me into a Mashed Potato.",
                time: "5:47"
            }
        ];
    };
    ChatRoomsPage.prototype.getItems = function (ev) {
        // Reset items back to all of the items
        this.initializeItems();
        // set val to the value of the ev target
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.items = this.items.filter(function (item) {
                return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    };
    ChatRoomsPage.prototype.openModal = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__components_modal_criar_sala_modal_criar_sala__["a" /* ModalCriarSalaComponent */]);
        modal.present();
    };
    ChatRoomsPage.prototype.goToChat = function (item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__chat_chat__["a" /* ChatPage */], {
            toUserName: item.name
        });
    };
    return ChatRoomsPage;
}());
ChatRoomsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-chat-rooms',template:/*ion-inline-start:"C:\Repositories\ChatApp2\src\pages\chat-rooms\chat-rooms.html"*/'<ion-header>\n\n  <ion-navbar>\n\n\n\n    <ion-title>Salas</ion-title>\n\n\n\n    <ion-buttons end>\n\n      <button ion-button icon-only (click)="openModal()">\n\n        <ion-icon name="ios-add"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n  <ion-searchbar (ionInput)="getItems($event)"></ion-searchbar>\n\n  \n\n  <ion-list>\n\n\n\n    <ion-item *ngFor="let item of items" (click)="goToChat(item)">\n\n      <ion-avatar item-start>\n\n        <img [src]="item.avatar">\n\n      </ion-avatar>\n\n      <h2>{{ item.name }}</h2>\n\n      <h4>{{ item.descricao }}</h4>\n\n      <h5>{{ item.topico }}</h5>\n\n      \n\n      <h5>10 participantes</h5>\n\n\n\n      <div>\n\n        <ion-avatar item-start class="participantes">\n\n          <img src="assets/img/avatar-ts-bullseye.png">\n\n        </ion-avatar>\n\n        <ion-avatar item-start class="participantes">\n\n          <img src="assets/img/avatar-ts-barbie.png">\n\n        </ion-avatar>\n\n        <ion-avatar item-start class="participantes">\n\n          <img src="assets/img/avatar-ts-squeeze.png">\n\n        </ion-avatar>\n\n        <ion-avatar item-start class="participantes">\n\n          <img src="assets/img/avatar-ts-sarge.png">\n\n        </ion-avatar>\n\n        <ion-avatar item-start class="participantes">\n\n          <img src="assets/img/avatar-ts-bopeep.png">\n\n        </ion-avatar>\n\n      </div>\n\n      \n\n      <!--<p>{{ item.message }}</p>-->\n\n      <ion-note item-end>{{ item.time }}</ion-note>\n\n    </ion-item>\n\n\n\n  </ion-list>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Repositories\ChatApp2\src\pages\chat-rooms\chat-rooms.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* ModalController */]])
], ChatRoomsPage);

//# sourceMappingURL=chat-rooms.js.map

/***/ }),

/***/ 202:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalCriarSalaComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ModalCriarSalaComponent = (function () {
    function ModalCriarSalaComponent(params, viewCtrl) {
        this.params = params;
        this.viewCtrl = viewCtrl;
    }
    ModalCriarSalaComponent.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return ModalCriarSalaComponent;
}());
ModalCriarSalaComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'modal-criar-sala',template:/*ion-inline-start:"C:\Repositories\ChatApp2\src\components\modal-criar-sala\modal-criar-sala.html"*/'<ion-header>\n\n  <ion-toolbar>\n\n    <ion-title>\n\n      Criar Sala\n\n    </ion-title>\n\n    <ion-buttons start>\n\n      <button ion-button (click)="dismiss()">\n\n        <span ion-text color="primary">Cancel</span>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-toolbar>\n\n</ion-header>\n\n\n\n<ion-content class="modal-criar-sala" padding>\n\n  <ion-list>\n\n\n\n    <ion-item>\n\n      <ion-label floating>Nome da sala</ion-label>\n\n      <ion-input type="text" value=""></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label floating>Descrição</ion-label>\n\n      <ion-input type="text" value=""></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label floating>Tópico</ion-label>\n\n      <ion-input type="text" value=""></ion-input>\n\n    </ion-item>\n\n\n\n  </ion-list>\n\n\n\n  <div padding>\n\n    <button ion-button block (click)="dismiss()">Criar</button>\n\n  </div>\n\n</ion-content>'/*ion-inline-end:"C:\Repositories\ChatApp2\src\components\modal-criar-sala\modal-criar-sala.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ViewController */]])
], ModalCriarSalaComponent);

//# sourceMappingURL=modal-criar-sala.js.map

/***/ }),

/***/ 203:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__setting_status_setting_status__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__setting_message_setting_message__ = __webpack_require__(205);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SettingsPage = (function () {
    function SettingsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    SettingsPage.prototype.goToStatus = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__setting_status_setting_status__["a" /* SettingStatusPage */]);
    };
    SettingsPage.prototype.goToMessage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__setting_message_setting_message__["a" /* SettingMessagePage */]);
    };
    return SettingsPage;
}());
SettingsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-settings',template:/*ion-inline-start:"C:\Repositories\ChatApp2\src\pages\settings\settings.html"*/'<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Configurações</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n  <ion-row class="row-content">\n\n    <ion-col>\n\n\n\n      <ion-row class="img-blur">\n\n        <ion-col>&nbsp;</ion-col>\n\n      </ion-row>\n\n\n\n      <ion-row class="img-opacity">\n\n        <ion-col>&nbsp;</ion-col>\n\n      </ion-row>\n\n\n\n      <ion-row padding class="img-info">\n\n        <ion-col text-center>\n\n          <img src="assets/img/avatar-ts-woody.png">\n\n          <h2>Woody</h2>\n\n          <p>Toy Story</p>\n\n        </ion-col>\n\n      </ion-row>\n\n\n\n    </ion-col>\n\n  </ion-row>\n\n  \n\n  <ion-list>\n\n    <button ion-item (click)="goToStatus()">\n\n      <ion-label>Status</ion-label>\n\n      <ion-note item-end>online</ion-note>\n\n    </button>\n\n\n\n    <button ion-item (click)="goToMessage()">\n\n      <ion-label>Mensagem</ion-label>\n\n      <ion-note item-end>Hello Word</ion-note>\n\n    </button>\n\n\n\n    <ion-item>\n\n      <ion-label>Notificações</ion-label>\n\n      <ion-toggle value="foo" checked="true"></ion-toggle>\n\n    </ion-item>\n\n  </ion-list>\n\n  \n\n</ion-content>\n\n'/*ion-inline-end:"C:\Repositories\ChatApp2\src\pages\settings\settings.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
], SettingsPage);

//# sourceMappingURL=settings.js.map

/***/ }),

/***/ 204:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingStatusPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SettingStatusPage = (function () {
    function SettingStatusPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    SettingStatusPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    return SettingStatusPage;
}());
SettingStatusPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-setting-status',template:/*ion-inline-start:"C:\Repositories\ChatApp2\src\pages\setting-status\setting-status.html"*/'<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Status</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n  <ion-list radio-group>\n\n    <!--<ion-list-header>\n\n      Alterar Status\n\n    </ion-list-header>-->\n\n\n\n    <ion-item>\n\n      <ion-label>Online</ion-label>\n\n      <ion-radio value="online" checked="true"></ion-radio>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label>Ocupado</ion-label>\n\n      <ion-radio value="ocupado"></ion-radio>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label>Ausente</ion-label>\n\n      <ion-radio value="ausente"></ion-radio>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label>Offline</ion-label>\n\n      <ion-radio value="offline"></ion-radio>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <!--<div padding>\n\n    <button ion-button block color="primary" (click)="goBack()">Salvar</button>\n\n  </div>-->\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Repositories\ChatApp2\src\pages\setting-status\setting-status.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
], SettingStatusPage);

//# sourceMappingURL=setting-status.js.map

/***/ }),

/***/ 205:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingMessagePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SettingMessagePage = (function () {
    function SettingMessagePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    SettingMessagePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SettingMessagePage');
    };
    SettingMessagePage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    return SettingMessagePage;
}());
SettingMessagePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-setting-message',template:/*ion-inline-start:"C:\Repositories\ChatApp2\src\pages\setting-message\setting-message.html"*/'<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Mensagem Pessoal</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <ion-list>\n\n\n\n    <ion-item>\n\n      <ion-label floating>Mensagem</ion-label>\n\n      <ion-input type="text" value="Hello Word"></ion-input>\n\n    </ion-item>\n\n\n\n  </ion-list>\n\n\n\n  <!--<div padding>\n\n    <button ion-button block (click)="goBack()">Salvar</button>\n\n  </div>-->\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Repositories\ChatApp2\src\pages\setting-message\setting-message.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
], SettingMessagePage);

//# sourceMappingURL=setting-message.js.map

/***/ }),

/***/ 206:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmojiProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the EmojiProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var EmojiProvider = (function () {
    function EmojiProvider(http) {
        this.http = http;
        console.log('Hello EmojiProvider Provider');
    }
    EmojiProvider.prototype.getEmojis = function () {
        var EMOJIS = "😀 😃 😄 😁 😆 😅 😂 🤣 ☺️ 😊 😇 🙂 🙃 😉 😌 😍 😘 😗 😙 😚 😋 😜 😝 😛 🤑 🤗 🤓 😎 🤡 🤠 😏 😒 😞 😔 😟 😕 🙁" +
            " ☹️ 😣 😖 😫 😩 😤 😠 😡 😶 😐 😑 😯 😦 😧 😮 😲 😵 😳 😱 😨 😰 😢 😥 🤤 😭 😓 😪 😴 🙄 🤔 🤥 😬 🤐 🤢 🤧 😷 🤒 🤕 😈 👿" +
            " 👹 👺 💩 👻 💀 ☠️ 👽 👾 🤖 🎃 😺 😸 😹 😻 😼 😽 🙀 😿 😾 👐 🙌 👏 🙏 🤝 👍 👎 👊 ✊ 🤛 🤜 🤞 ✌️ 🤘 👌 👈 👉 👆 👇 ☝️ ✋ 🤚" +
            " 🖐 🖖 👋 🤙 💪 🖕 ✍️ 🤳 💅 🖖 💄 💋 👄 👅 👂 👃 👣 👁 👀 🗣 👤 👥 👶 👦 👧 👨 👩 👱‍♀️ 👱 👴 👵 👲 👳‍♀️ 👳 👮‍♀️ 👮 👷‍♀️ 👷" +
            " 💂‍♀️ 💂 🕵️‍♀️ 🕵️ 👩‍⚕️ 👨‍⚕️ 👩‍🌾 👨‍🌾 👩‍🍳 👨‍🍳 👩‍🎓 👨‍🎓 👩‍🎤 👨‍🎤 👩‍🏫 👨‍🏫 👩‍🏭 👨‍🏭 👩‍💻 👨‍💻 👩‍💼 👨‍💼 👩‍🔧 👨‍🔧 👩‍🔬 👨‍🔬" +
            " 👩‍🎨 👨‍🎨 👩‍🚒 👨‍🚒 👩‍✈️ 👨‍✈️ 👩‍🚀 👨‍🚀 👩‍⚖️ 👨‍⚖️ 🤶 🎅 👸 🤴 👰 🤵 👼 🤰 🙇‍♀️ 🙇 💁 💁‍♂️ 🙅 🙅‍♂️ 🙆 🙆‍♂️ 🙋 🙋‍♂️ 🤦‍♀️ 🤦‍♂️ 🤷‍♀" +
            "️ 🤷‍♂️ 🙎 🙎‍♂️ 🙍 🙍‍♂️ 💇 💇‍♂️ 💆 💆‍♂️ 🕴 💃 🕺 👯 👯‍♂️ 🚶‍♀️ 🚶 🏃‍♀️ 🏃 👫 👭 👬 💑 👩‍❤️‍👩 👨‍❤️‍👨 💏 👩‍❤️‍💋‍👩 👨‍❤️‍💋‍👨 👪 👨‍👩‍👧" +
            " 👨‍👩‍👧‍👦 👨‍👩‍👦‍👦 👨‍👩‍👧‍👧 👩‍👩‍👦 👩‍👩‍👧 👩‍👩‍👧‍👦 👩‍👩‍👦‍👦 👩‍👩‍👧‍👧 👨‍👨‍👦 👨‍👨‍👧 👨‍👨‍👧‍👦 👨‍👨‍👦‍👦 👨‍👨‍👧‍👧 👩‍👦 👩‍👧" +
            " 👩‍👧‍👦 👩‍👦‍👦 👩‍👧‍👧 👨‍👦 👨‍👧 👨‍👧‍👦 👨‍👦‍👦 👨‍👧‍👧 👚 👕 👖 👔 👗 👙 👘 👠 👡 👢 👞 👟 👒 🎩 🎓 👑 ⛑ 🎒 👝 👛 👜 💼 👓" +
            " 🕶 🌂 ☂️";
        var EmojiArr = EMOJIS.split(' ');
        var groupNum = Math.ceil(EmojiArr.length / (24));
        var items = [];
        for (var i = 0; i < groupNum; i++) {
            items.push(EmojiArr.slice(i * 24, (i + 1) * 24));
        }
        return items;
    };
    return EmojiProvider;
}());
EmojiProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]])
], EmojiProvider);

//# sourceMappingURL=emoji.js.map

/***/ }),

/***/ 322:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(323);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(341);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 341:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(381);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_chat_rooms_chat_rooms__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_contact_contact__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_conversations_conversations__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_chat_chat__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_settings_settings__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_setting_status_setting_status__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_setting_message_setting_message__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_tabs_tabs__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_status_bar__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_splash_screen__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__components_modal_criar_sala_modal_criar_sala__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__components_modal_buscar_contato_modal_buscar_contato__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__providers_chat_service_chat_service__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__providers_emoji_emoji__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pipes_pipes_module__ = __webpack_require__(392);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__components_emoji_picker_emoji_picker__ = __webpack_require__(396);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__angular_http__ = __webpack_require__(101);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




//import * as Strophe from 'strophe.js';

















//import 'rxjs/add/operator/map';
//import 'rxjs/add/operator/toPromise';
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_4__pages_chat_rooms_chat_rooms__["a" /* ChatRoomsPage */],
            __WEBPACK_IMPORTED_MODULE_5__pages_contact_contact__["a" /* ContactPage */],
            __WEBPACK_IMPORTED_MODULE_6__pages_conversations_conversations__["a" /* ConversationsPage */],
            __WEBPACK_IMPORTED_MODULE_7__pages_chat_chat__["a" /* ChatPage */],
            __WEBPACK_IMPORTED_MODULE_8__pages_settings_settings__["a" /* SettingsPage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_setting_status_setting_status__["a" /* SettingStatusPage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_setting_message_setting_message__["a" /* SettingMessagePage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_tabs_tabs__["a" /* TabsPage */],
            __WEBPACK_IMPORTED_MODULE_14__components_modal_criar_sala_modal_criar_sala__["a" /* ModalCriarSalaComponent */],
            __WEBPACK_IMPORTED_MODULE_15__components_modal_buscar_contato_modal_buscar_contato__["a" /* ModalBuscarContatoComponent */],
            __WEBPACK_IMPORTED_MODULE_19__components_emoji_picker_emoji_picker__["a" /* EmojiPickerComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {
                backButtonText: "Voltar"
            }),
            __WEBPACK_IMPORTED_MODULE_18__pipes_pipes_module__["a" /* PipesModule */],
            __WEBPACK_IMPORTED_MODULE_20__angular_http__["b" /* HttpModule */]
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_4__pages_chat_rooms_chat_rooms__["a" /* ChatRoomsPage */],
            __WEBPACK_IMPORTED_MODULE_5__pages_contact_contact__["a" /* ContactPage */],
            __WEBPACK_IMPORTED_MODULE_6__pages_conversations_conversations__["a" /* ConversationsPage */],
            __WEBPACK_IMPORTED_MODULE_7__pages_chat_chat__["a" /* ChatPage */],
            __WEBPACK_IMPORTED_MODULE_8__pages_settings_settings__["a" /* SettingsPage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_setting_status_setting_status__["a" /* SettingStatusPage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_setting_message_setting_message__["a" /* SettingMessagePage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_tabs_tabs__["a" /* TabsPage */],
            __WEBPACK_IMPORTED_MODULE_14__components_modal_criar_sala_modal_criar_sala__["a" /* ModalCriarSalaComponent */],
            __WEBPACK_IMPORTED_MODULE_15__components_modal_buscar_contato_modal_buscar_contato__["a" /* ModalBuscarContatoComponent */],
            __WEBPACK_IMPORTED_MODULE_19__components_emoji_picker_emoji_picker__["a" /* EmojiPickerComponent */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_12__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_13__ionic_native_splash_screen__["a" /* SplashScreen */],
            { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicErrorHandler */] },
            __WEBPACK_IMPORTED_MODULE_16__providers_chat_service_chat_service__["b" /* ChatServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_17__providers_emoji_emoji__["a" /* EmojiProvider */]
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 381:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__["a" /* TabsPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    return MyApp;
}());
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"C:\Repositories\ChatApp2\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"C:\Repositories\ChatApp2\src\app\app.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 392:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PipesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pipes_relative_time_relative_time__ = __webpack_require__(393);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var PipesModule = (function () {
    function PipesModule() {
    }
    return PipesModule;
}());
PipesModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [__WEBPACK_IMPORTED_MODULE_1__pipes_relative_time_relative_time__["a" /* RelativeTimePipe */]],
        imports: [],
        exports: [__WEBPACK_IMPORTED_MODULE_1__pipes_relative_time_relative_time__["a" /* RelativeTimePipe */]]
    })
], PipesModule);

//# sourceMappingURL=pipes.module.js.map

/***/ }),

/***/ 393:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RelativeTimePipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


/**
 * Generated class for the RelativeTimePipe pipe.
 *
 * See https://angular.io/docs/ts/latest/guide/pipes.html for more info on
 * Angular Pipes.
 */
var RelativeTimePipe = (function () {
    function RelativeTimePipe() {
    }
    /**
     * Takes a value and makes it lowercase.
     */
    RelativeTimePipe.prototype.transform = function (value) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        return __WEBPACK_IMPORTED_MODULE_1_moment__(value).toNow();
    };
    return RelativeTimePipe;
}());
RelativeTimePipe = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["W" /* Pipe */])({
        name: 'relativeTime',
    })
], RelativeTimePipe);

//# sourceMappingURL=relative-time.js.map

/***/ }),

/***/ 395:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 207,
	"./af.js": 207,
	"./ar": 208,
	"./ar-dz": 209,
	"./ar-dz.js": 209,
	"./ar-kw": 210,
	"./ar-kw.js": 210,
	"./ar-ly": 211,
	"./ar-ly.js": 211,
	"./ar-ma": 212,
	"./ar-ma.js": 212,
	"./ar-sa": 213,
	"./ar-sa.js": 213,
	"./ar-tn": 214,
	"./ar-tn.js": 214,
	"./ar.js": 208,
	"./az": 215,
	"./az.js": 215,
	"./be": 216,
	"./be.js": 216,
	"./bg": 217,
	"./bg.js": 217,
	"./bn": 218,
	"./bn.js": 218,
	"./bo": 219,
	"./bo.js": 219,
	"./br": 220,
	"./br.js": 220,
	"./bs": 221,
	"./bs.js": 221,
	"./ca": 222,
	"./ca.js": 222,
	"./cs": 223,
	"./cs.js": 223,
	"./cv": 224,
	"./cv.js": 224,
	"./cy": 225,
	"./cy.js": 225,
	"./da": 226,
	"./da.js": 226,
	"./de": 227,
	"./de-at": 228,
	"./de-at.js": 228,
	"./de-ch": 229,
	"./de-ch.js": 229,
	"./de.js": 227,
	"./dv": 230,
	"./dv.js": 230,
	"./el": 231,
	"./el.js": 231,
	"./en-au": 232,
	"./en-au.js": 232,
	"./en-ca": 233,
	"./en-ca.js": 233,
	"./en-gb": 234,
	"./en-gb.js": 234,
	"./en-ie": 235,
	"./en-ie.js": 235,
	"./en-nz": 236,
	"./en-nz.js": 236,
	"./eo": 237,
	"./eo.js": 237,
	"./es": 238,
	"./es-do": 239,
	"./es-do.js": 239,
	"./es.js": 238,
	"./et": 240,
	"./et.js": 240,
	"./eu": 241,
	"./eu.js": 241,
	"./fa": 242,
	"./fa.js": 242,
	"./fi": 243,
	"./fi.js": 243,
	"./fo": 244,
	"./fo.js": 244,
	"./fr": 245,
	"./fr-ca": 246,
	"./fr-ca.js": 246,
	"./fr-ch": 247,
	"./fr-ch.js": 247,
	"./fr.js": 245,
	"./fy": 248,
	"./fy.js": 248,
	"./gd": 249,
	"./gd.js": 249,
	"./gl": 250,
	"./gl.js": 250,
	"./gom-latn": 251,
	"./gom-latn.js": 251,
	"./he": 252,
	"./he.js": 252,
	"./hi": 253,
	"./hi.js": 253,
	"./hr": 254,
	"./hr.js": 254,
	"./hu": 255,
	"./hu.js": 255,
	"./hy-am": 256,
	"./hy-am.js": 256,
	"./id": 257,
	"./id.js": 257,
	"./is": 258,
	"./is.js": 258,
	"./it": 259,
	"./it.js": 259,
	"./ja": 260,
	"./ja.js": 260,
	"./jv": 261,
	"./jv.js": 261,
	"./ka": 262,
	"./ka.js": 262,
	"./kk": 263,
	"./kk.js": 263,
	"./km": 264,
	"./km.js": 264,
	"./kn": 265,
	"./kn.js": 265,
	"./ko": 266,
	"./ko.js": 266,
	"./ky": 267,
	"./ky.js": 267,
	"./lb": 268,
	"./lb.js": 268,
	"./lo": 269,
	"./lo.js": 269,
	"./lt": 270,
	"./lt.js": 270,
	"./lv": 271,
	"./lv.js": 271,
	"./me": 272,
	"./me.js": 272,
	"./mi": 273,
	"./mi.js": 273,
	"./mk": 274,
	"./mk.js": 274,
	"./ml": 275,
	"./ml.js": 275,
	"./mr": 276,
	"./mr.js": 276,
	"./ms": 277,
	"./ms-my": 278,
	"./ms-my.js": 278,
	"./ms.js": 277,
	"./my": 279,
	"./my.js": 279,
	"./nb": 280,
	"./nb.js": 280,
	"./ne": 281,
	"./ne.js": 281,
	"./nl": 282,
	"./nl-be": 283,
	"./nl-be.js": 283,
	"./nl.js": 282,
	"./nn": 284,
	"./nn.js": 284,
	"./pa-in": 285,
	"./pa-in.js": 285,
	"./pl": 286,
	"./pl.js": 286,
	"./pt": 287,
	"./pt-br": 288,
	"./pt-br.js": 288,
	"./pt.js": 287,
	"./ro": 289,
	"./ro.js": 289,
	"./ru": 290,
	"./ru.js": 290,
	"./sd": 291,
	"./sd.js": 291,
	"./se": 292,
	"./se.js": 292,
	"./si": 293,
	"./si.js": 293,
	"./sk": 294,
	"./sk.js": 294,
	"./sl": 295,
	"./sl.js": 295,
	"./sq": 296,
	"./sq.js": 296,
	"./sr": 297,
	"./sr-cyrl": 298,
	"./sr-cyrl.js": 298,
	"./sr.js": 297,
	"./ss": 299,
	"./ss.js": 299,
	"./sv": 300,
	"./sv.js": 300,
	"./sw": 301,
	"./sw.js": 301,
	"./ta": 302,
	"./ta.js": 302,
	"./te": 303,
	"./te.js": 303,
	"./tet": 304,
	"./tet.js": 304,
	"./th": 305,
	"./th.js": 305,
	"./tl-ph": 306,
	"./tl-ph.js": 306,
	"./tlh": 307,
	"./tlh.js": 307,
	"./tr": 308,
	"./tr.js": 308,
	"./tzl": 309,
	"./tzl.js": 309,
	"./tzm": 310,
	"./tzm-latn": 311,
	"./tzm-latn.js": 311,
	"./tzm.js": 310,
	"./uk": 312,
	"./uk.js": 312,
	"./ur": 313,
	"./ur.js": 313,
	"./uz": 314,
	"./uz-latn": 315,
	"./uz-latn.js": 315,
	"./uz.js": 314,
	"./vi": 316,
	"./vi.js": 316,
	"./x-pseudo": 317,
	"./x-pseudo.js": 317,
	"./yo": 318,
	"./yo.js": 318,
	"./zh-cn": 319,
	"./zh-cn.js": 319,
	"./zh-hk": 320,
	"./zh-hk.js": 320,
	"./zh-tw": 321,
	"./zh-tw.js": 321
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 395;

/***/ }),

/***/ 396:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export EMOJI_PICKER_VALUE_ACCESSOR */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmojiPickerComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_emoji_emoji__ = __webpack_require__(206);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the EmojiPickerComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
var EMOJI_PICKER_VALUE_ACCESSOR = {
    provide: __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* NG_VALUE_ACCESSOR */],
    useExisting: Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_20" /* forwardRef */])(function () { return EmojiPickerComponent; }),
    multi: true
};
var EmojiPickerComponent = (function () {
    function EmojiPickerComponent(emojiProvider) {
        this.emojiArr = [];
        this.emojiArr = emojiProvider.getEmojis();
    }
    EmojiPickerComponent.prototype.writeValue = function (obj) {
        this._content = obj;
        console.log(this._content);
    };
    EmojiPickerComponent.prototype.registerOnChange = function (fn) {
        this._onChanged = fn;
        this.setValue(this._content);
    };
    EmojiPickerComponent.prototype.registerOnTouched = function (fn) {
        this._onTouched = fn;
    };
    EmojiPickerComponent.prototype.setValue = function (val) {
        this._content += val;
        if (this._content) {
            this._onChanged(this._content);
        }
    };
    return EmojiPickerComponent;
}());
EmojiPickerComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'emoji-picker',
        providers: [EMOJI_PICKER_VALUE_ACCESSOR],template:/*ion-inline-start:"C:\Repositories\ChatApp2\src\components\emoji-picker\emoji-picker.html"*/'<!-- Generated template for the EmojiPickerComponent component -->\n\n<div class="emoji-picker">\n\n  <div class="emoji-items">\n\n    <ion-slides pager>\n\n\n\n      <ion-slide *ngFor="let items of emojiArr">\n\n        <span class="emoji-item"\n\n              (click)="setValue(item)"\n\n              *ngFor="let item of items">\n\n          {{item}}\n\n        </span>\n\n      </ion-slide>\n\n\n\n    </ion-slides>\n\n  </div>\n\n</div>\n\n'/*ion-inline-end:"C:\Repositories\ChatApp2\src\components\emoji-picker\emoji-picker.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_emoji_emoji__["a" /* EmojiProvider */]])
], EmojiPickerComponent);

//# sourceMappingURL=emoji-picker.js.map

/***/ }),

/***/ 50:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_chat_service_chat_service__ = __webpack_require__(100);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//import { EmojiProvider } from "../../providers/emoji/emoji";
//declare var Strophe:any;
var ChatPage = (function () {
    function ChatPage(navParams, chatService, events) {
        var _this = this;
        this.navParams = navParams;
        this.chatService = chatService;
        this.events = events;
        this.msgList = [];
        this.editorMsg = '';
        this._isOpenEmojiPicker = false;
        // Get the navParams toUserId parameter
        this.toUserId = '210000198410281948'; //navParams.get('toUserId');
        this.toUserName = navParams.get('toUserName'); //'Agência Estado'
        // Get mock user information
        this.chatService.getUserInfo()
            .then(function (res) {
            _this.userId = res.userId;
            _this.userName = res.userName;
            _this.userImgUrl = res.userImgUrl;
        });
    }
    ChatPage.prototype.ionViewDidLoad = function () {
        // this.switchEmojiPicker();
    };
    ChatPage.prototype.ionViewDidLeave = function () {
        //this.navController.setRoot(/*sua pagina inicia da tab*/);
    };
    ChatPage.prototype.ionViewWillLeave = function () {
        // unsubscribe
        this.events.unsubscribe('chat:received');
    };
    ChatPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        //get message list
        this.getMsg()
            .then(function () {
            _this.scrollToBottom();
        });
        // Subscribe to received  new message events
        this.events.subscribe('chat:received', function (msg, time) {
            _this.pushNewMsg(msg);
        });
    };
    ChatPage.prototype._focus = function () {
        this._isOpenEmojiPicker = false;
        this.content.resize();
        this.scrollToBottom();
    };
    ChatPage.prototype.switchEmojiPicker = function () {
        this._isOpenEmojiPicker = !this._isOpenEmojiPicker;
        if (!this._isOpenEmojiPicker) {
            this.messageInput.setFocus();
        }
        this.content.resize();
        this.scrollToBottom();
    };
    /**
     * @name getMsg
     * @returns {Promise<ChatMessage[]>}
     */
    ChatPage.prototype.getMsg = function () {
        var _this = this;
        // Get mock message list
        return this.chatService
            .getMsgList()
            .then(function (res) {
            _this.msgList = res;
        })
            .catch(function (err) {
            console.log(err);
        });
    };
    /**
     * @name sendMsg
     */
    ChatPage.prototype.sendMsg = function () {
        var _this = this;
        if (!this.editorMsg.trim())
            return;
        // Mock message
        var id = Date.now().toString();
        var newMsg = new __WEBPACK_IMPORTED_MODULE_2__providers_chat_service_chat_service__["a" /* ChatMessage */]();
        newMsg.messageId = Date.now().toString();
        newMsg.userId = this.userId;
        newMsg.userName = this.userName;
        newMsg.userImgUrl = this.userImgUrl;
        newMsg.toUserId = this.toUserId;
        newMsg.time = Date.now();
        newMsg.message = this.editorMsg;
        newMsg.status = 'pending';
        this.pushNewMsg(newMsg);
        this.editorMsg = '';
        if (!this._isOpenEmojiPicker) {
            this.messageInput.setFocus();
        }
        this.chatService.sendMsg(newMsg)
            .then(function () {
            var index = _this.getMsgIndexById(id);
            if (index !== -1) {
                _this.msgList[index].status = 'success';
            }
        });
    };
    /**
     * @name pushNewMsg
     * @param msg
     */
    ChatPage.prototype.pushNewMsg = function (msg) {
        // Verify user relationships
        if (msg.userId === this.userId && msg.toUserId === this.toUserId) {
            this.msgList.push(msg);
        }
        else if (msg.toUserId === this.userId && msg.userId === this.toUserId) {
            this.msgList.push(msg);
        }
        this.scrollToBottom();
    };
    ChatPage.prototype.getMsgIndexById = function (id) {
        return this.msgList.findIndex(function (e) { return e.messageId === id; });
    };
    ChatPage.prototype.scrollToBottom = function () {
        var _this = this;
        setTimeout(function () {
            if (_this.content.scrollToBottom) {
                _this.content.scrollToBottom();
            }
        }, 400);
    };
    return ChatPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* Content */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* Content */])
], ChatPage.prototype, "content", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* ViewChild */])('chat_input'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* TextInput */])
], ChatPage.prototype, "messageInput", void 0);
ChatPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-chat',template:/*ion-inline-start:"C:\Repositories\ChatApp2\src\pages\chat\chat.html"*/'<!--\n\n  Generated template for the Chat page.\n\n\n\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>{{toUserName}}</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n\n\n  <div class="message-wrap">\n\n\n\n    <div *ngFor="let msg of msgList"\n\n         class="message"\n\n         [class.left]=" msg.userId === toUserId "\n\n         [class.right]=" msg.userId === userId ">\n\n      <img class="user-img" [src]="msg.userImgUrl" alt="" src="">\n\n      <ion-spinner name="dots" *ngIf="msg.status === \'pending\'"></ion-spinner>\n\n      <div class="msg-detail">\n\n        <div class="msg-info">\n\n          <p><!--{{msg.userName}}&nbsp;&nbsp;&nbsp;-->{{msg.time | relativeTime}}</p>\n\n        </div>\n\n        <div class="msg-content">\n\n          <span class="triangle"></span>\n\n          <p class="line-breaker ">{{msg.message}}</p>\n\n        </div>\n\n      </div>\n\n    </div>\n\n\n\n  </div>\n\n\n\n</ion-content>\n\n\n\n<ion-footer no-border [style.height]="_isOpenEmojiPicker ? \'255px\' : \'55px\'">\n\n  <ion-grid class="input-wrap">\n\n    <ion-row>\n\n\n\n      <ion-col col-2>\n\n        <button ion-button clear icon-only item-right\n\n                (click)="switchEmojiPicker()" >\n\n          <ion-icon  name="md-happy"></ion-icon>\n\n        </button>\n\n      </ion-col>\n\n      <ion-col col-8>\n\n        <ion-textarea\n\n                #chat_input\n\n                placeholder="Mensagem"\n\n                [(ngModel)]="editorMsg"\n\n                (keyup.enter)="sendMsg()"\n\n                (focus)="_focus()"\n\n        ></ion-textarea>\n\n      </ion-col>\n\n      <ion-col col-2>\n\n        <button ion-button clear icon-only item-right\n\n                (click)="sendMsg()" >\n\n          <ion-icon  name="ios-send" ios="ios-send" md="md-send"></ion-icon>\n\n        </button>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n  <emoji-picker *ngIf="_isOpenEmojiPicker" [(ngModel)]="editorMsg"></emoji-picker>\n\n</ion-footer>'/*ion-inline-end:"C:\Repositories\ChatApp2\src\pages\chat\chat.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_chat_service_chat_service__["b" /* ChatServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Events */]])
], ChatPage);

//# sourceMappingURL=chat.js.map

/***/ })

},[322]);
//# sourceMappingURL=main.js.map